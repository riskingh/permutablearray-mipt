//
//  main.cpp
//  PermutableArray
//
//  Created by Максим Гришкин on 12/03/15.
//  Copyright (c) 2015 Максим Гришкин. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <cstdlib>

#include "PermutableArray.h"
#include "RopePermutableArray.h"
#include "StdPermutableArray.h"

#include "Tests.h"

#include <limits>

int main(int argc, const char * argv[]) {
    NPermutableArray::CRopePermutableArray *ropeArray = new NPermutableArray::CRopePermutableArray();
    NPermutableArray::CStdPermutableArray *stdArray = new NPermutableArray::CStdPermutableArray();
    NTests::areEqual(ropeArray, stdArray, 10000, 200000, -1e9, 1e9);

//    srand(0);
//    freopen("input.txt", "r", stdin);
//    std::vector<NTests::CQuery> test;
//    size_t testCount, testNumber, firstSizeT, secondSizeT;
//    std::cin >> testCount;
//    int type, value;
//    for (testNumber = 0; testNumber < testCount; ++testNumber) {
//        std::cin >> type >> firstSizeT >> secondSizeT >> value;
//        test.push_back(NTests::CQuery((NTests::EQueryType)type, firstSizeT, secondSizeT, value));
//    }
//    
//    std::vector<long long> answers;
//    answers = NTests::generateAnswers(ropeArray, test, true);
//    for (long long ans : answers)
//        std::cout << ans << " ";
//    std::cout << "\n";
//    
//    answers = NTests::generateAnswers(stdArray, test, true);
//    for (long long ans : answers)
//        std::cout << ans << " ";
//    std::cout << "\n";
    return 0;
}
