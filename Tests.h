//
//  Tests.h
//  PermutableArray
//
//  Created by Максим Гришкин on 15/03/15.
//  Copyright (c) 2015 Максим Гришкин. All rights reserved.
//

#ifndef __PermutableArray__Tests__
#define __PermutableArray__Tests__

#include <stdio.h>
#include <ctime>
#include <random>
#include <vector>
#include <iostream>
#include <cassert>
#include <cstdlib>

#include "PermutableArray.h"
#include "RopePermutableArray.h"
#include "StdPermutableArray.h"

namespace NTests {
    std::default_random_engine mainGenerator((unsigned int)time(0));
//    std::default_random_engine mainGenerator;
    int getRandomInt(int _min, int _max) {
        std::uniform_int_distribution<int> distribution(_min, _max);
        return distribution(mainGenerator);
    }
    
    enum EQueryType {
        QT_SubsegmentSum,
        QT_Insert,
        QT_Assign,
        QT_NextPermutation,
        QT_Size
    };
    
    class CQuery {
    public:
        EQueryType type;
        size_t firstSizeT, secondSizeT;
        int value;
        
//        CQuery(EQueryType _type, size_t _firstSizeT, size_t _secondSizeT, int _value)
//        : type(_type), firstSizeT(_firstSizeT), secondSizeT(_secondSizeT), value(_value) {
//        }
    };
    
    long long getRespond(NPermutableArray::IPermutableArray *_permutableArray, const CQuery &_query, bool &_void) {
        long long result = 0;
        switch (_query.type) {
            case QT_SubsegmentSum:
                _void = false;
                result = _permutableArray->subsegmentSum(_query.firstSizeT, _query.secondSizeT);
                break;
            case QT_Insert:
                _void = true;
                _permutableArray->insert(_query.firstSizeT, _query.value);
                break;
            case QT_Assign:
                _void = true;
                _permutableArray->assign(_query.firstSizeT, _query.value);
                break;
            case QT_NextPermutation:
                _void = true;
                _permutableArray->nextPermutation(_query.firstSizeT, _query.secondSizeT);
                break;
            default:
                _void = true;
                break;
        }
        return result;
    }
    
    std::vector<CQuery> generateTest(size_t _queryCount, int _minValue, int _maxValue) {
        std::vector<CQuery> test;
        test.push_back(CQuery{QT_Insert, 0, 0, getRandomInt(_minValue, _maxValue)});
        CQuery query{QT_Size, 0, 0, 0};
        size_t arraySize = 1;
        for (; _queryCount > 0; --_queryCount) {
            query.type = (EQueryType)getRandomInt(0, (int)QT_Size - 1);
            query.value = getRandomInt(_minValue, _maxValue);
            if (query.type == QT_Insert) {
                query.firstSizeT = (size_t)getRandomInt(0, (int)arraySize);
                arraySize++;
            }
            else {
                query.firstSizeT = (size_t)getRandomInt(0, (int)arraySize - 1);
                query.secondSizeT = (size_t)getRandomInt((int)query.firstSizeT + 1, (int)arraySize);
            }
            test.push_back(query);
        }
        return test;
    }
    
    std::vector<long long> generateAnswers(NPermutableArray::IPermutableArray *_permutableArray, const std::vector<CQuery> &_test, bool verbose = false) {
        std::vector<long long> result;
        bool isVoid;
        long long respond;
        if (verbose)
            _permutableArray->show();
        for (const auto &query: _test) {
            respond = getRespond(_permutableArray, query, isVoid);
            if (verbose)
                _permutableArray->show();
            if (!isVoid)
                result.push_back(respond);
        }
        if (verbose)
            std::cout << "\n";
        return result;
    }
    
//    template <class _T>
//    void clearVector(std::vector<_T> &_vector) {
//        std::vector<_T> temp;
//        swap(_vector, temp);
//    }
    
    bool areEqual(NPermutableArray::IPermutableArray *_firstPermutableArray, NPermutableArray::IPermutableArray *_secondPermutableArray, size_t _testCount, size_t _queryCount, int _minValue, int _maxValue) {
        size_t equalCount = 0, testNumber, answerNumber;
        std::vector<CQuery> test;
        std::vector<long long> answers1, answers2;
        bool equal;
        for (testNumber = 1; testNumber <= _testCount; ++testNumber) {
            _firstPermutableArray->clear();
            _secondPermutableArray->clear();
            test = generateTest(_queryCount, _minValue, _maxValue);
            answers1 = generateAnswers(_firstPermutableArray, test);
            answers2 = generateAnswers(_secondPermutableArray, test);
            equal = (answers1.size() == answers2.size());
            for (answerNumber = 0; answerNumber < answers1.size() && equal; ++answerNumber)
                equal &= (answers1[answerNumber] == answers2[answerNumber]);
            equalCount += equal;
            
            if (!equal) {
                std::cout << "NOT EQUAL\n";
                std::cout << test.size() << "\n";
                for (const auto &query: test)
                    std::cout << query.type << " " << query.firstSizeT << " " << query.secondSizeT << " " << query.value << "\n";
                std::cout << "\n";
                for (long long ans : answers1)
                    std::cout << ans << " ";
                std::cout << "\n";
                for (long long ans : answers2)
                    std::cout << ans << " ";
                std::cout << "\n";
                return 0;
            }
        }
        std::cout << "EQUAL " << equalCount << "/" << _testCount << "\n";
        return equalCount == _testCount;
    }
}

#endif /* defined(__PermutableArray__Tests__) */
