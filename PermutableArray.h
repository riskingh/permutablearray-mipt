//
//  PermutableArray.h
//  PermutableArray
//
//  Created by Максим Гришкин on 12/03/15.
//  Copyright (c) 2015 Максим Гришкин. All rights reserved.
//

#ifndef __PermutableArray__PermutableArray__
#define __PermutableArray__PermutableArray__

#include <stdio.h>

namespace NPermutableArray {
    class IPermutableArray {
    public:
        virtual long long subsegmentSum(size_t, size_t) = 0;
        virtual void insert(size_t, int) = 0;
        virtual void assign(size_t, int) = 0;
        virtual void nextPermutation(size_t, size_t) = 0;
        virtual void show() = 0;
        virtual void clear() = 0;
        virtual ~IPermutableArray() {}
    };
}

#endif /* defined(__PermutableArray__PermutableArray__) */
