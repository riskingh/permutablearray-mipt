//
//  RopePermutableArray.h
//  PermutableArray
//
//  Created by Максим Гришкин on 12/03/15.
//  Copyright (c) 2015 Максим Гришкин. All rights reserved.
//

#ifndef __PermutableArray__RopePermutableArray__
#define __PermutableArray__RopePermutableArray__

#include <stdio.h>
#include <cstdlib>
#include <algorithm>
#include <iostream>

#include "PermutableArray.h"

namespace NPermutableArray {
    class CRopePermutableArray: public IPermutableArray {
    private:
        class CRopeNode {
        public:
            CRopeNode *left, *right;
            size_t size;
            int value, heapKey;
            bool reversed;
            
            long long sum;
            size_t maxNotDescendingPrefixLength, maxNotAscendingSuffixLength;
            int firstValue, lastValue;
            
            CRopeNode(int _value);
            ~CRopeNode();
            void assign(int _value);
        };
        
        // getters, *_node can be NULL
        size_t size(CRopeNode *_node);
        long long sum(CRopeNode *_node);
        size_t maxNotDescendingPrefixLength(CRopeNode *_node);
        size_t maxNotAscendingSuffixLength(CRopeNode *_node);
        
        // getters, *_node can not be NULL
        int firstValue(CRopeNode *_node);
        int lastValue(CRopeNode *_node);
        
        void push(CRopeNode *_node);
        CRopeNode *update(CRopeNode *_node);
        CRopeNode *merge(CRopeNode *_leftNode, CRopeNode *_rightNode);
        void split(CRopeNode *_node, size_t _leftSize, CRopeNode *&_left, CRopeNode *&_right);
        
        // only for not descending rope
        void splitByValue(CRopeNode *_node, int _value, CRopeNode *&_left, CRopeNode *&_right);
        CRopeNode *replaceMinGreater(CRopeNode *&_node, CRopeNode *_elem);
        
        void show(CRopeNode *_node);
        
        CRopeNode *root;

    public:
        CRopePermutableArray();
        ~CRopePermutableArray();
        
        long long subsegmentSum(size_t _leftEnd, size_t _rightEnd);
        void insert(size_t _position, int _value);
        void assign(size_t _position, int _value);
        void nextPermutation(size_t _leftEnd, size_t _rightEnd);
        
        void show();
        void clear();
    };
}

#endif /* defined(__PermutableArray__RopePermutableArray__) */
