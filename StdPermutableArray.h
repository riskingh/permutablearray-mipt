//
//  StdPermutableArray.h
//  PermutableArray
//
//  Created by Максим Гришкин on 15/03/15.
//  Copyright (c) 2015 Максим Гришкин. All rights reserved.
//

#ifndef __PermutableArray__StdPermutableArray__
#define __PermutableArray__StdPermutableArray__

#include <stdio.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <numeric>

#include "PermutableArray.h"

namespace NPermutableArray {
    class CStdPermutableArray: public IPermutableArray {
    private:
        std::vector<int> data;
    public:
        CStdPermutableArray();
        ~CStdPermutableArray();
        
        long long subsegmentSum(size_t _leftEnd, size_t _rightEnd);
        void insert(size_t _position, int _value);
        void assign(size_t _position, int _value);
        void nextPermutation(size_t _leftEnd, size_t _rightEnd);
        
        void show();
        void clear();
    };
}

#endif /* defined(__PermutableArray__StdPermutableArray__) */
