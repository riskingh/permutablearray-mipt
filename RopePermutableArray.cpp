//
//  RopePermutableArray.cpp
//  PermutableArray
//
//  Created by Максим Гришкин on 12/03/15.
//  Copyright (c) 2015 Максим Гришкин. All rights reserved.
//

#include "RopePermutableArray.h"

#include <cassert>

namespace NPermutableArray {
    CRopePermutableArray::CRopeNode::CRopeNode(int _value)
    : left(NULL), right(NULL), size(1), value(_value), heapKey(rand()), reversed(false), sum(_value), maxNotDescendingPrefixLength(1), maxNotAscendingSuffixLength(1), firstValue(_value), lastValue(_value) {}
    
    CRopePermutableArray::CRopeNode::~CRopeNode() {
        delete left;
        delete right;
    }
    
    void CRopePermutableArray::CRopeNode::assign(int _value) {
        value = _value;
        sum = _value;
        firstValue = lastValue = _value;
    }
    
    
    size_t CRopePermutableArray::size(CRopePermutableArray::CRopeNode *_node) {
        return _node ? _node->size : 0;
    }
    
    long long CRopePermutableArray::sum(CRopePermutableArray::CRopeNode *_node) {
        return _node ? _node->sum : 0;
    }
    
    size_t CRopePermutableArray::maxNotDescendingPrefixLength(CRopePermutableArray::CRopeNode *_node) {
        if (!_node)
            return 0;
        return _node->reversed ? _node->maxNotAscendingSuffixLength : _node->maxNotDescendingPrefixLength;
    }
    
    size_t CRopePermutableArray::maxNotAscendingSuffixLength(CRopePermutableArray::CRopeNode *_node) {
        if (!_node)
            return 0;
        return _node->reversed ? _node->maxNotDescendingPrefixLength : _node->maxNotAscendingSuffixLength;
    }
    
    int CRopePermutableArray::firstValue(CRopePermutableArray::CRopeNode *_node) {
        return _node->reversed ? _node->lastValue : _node->firstValue;
    }
    
    int CRopePermutableArray::lastValue(CRopePermutableArray::CRopeNode *_node) {
        return _node->reversed ? _node->firstValue : _node->lastValue;
    }
    
    
    void CRopePermutableArray::push(CRopePermutableArray::CRopeNode *_node) {
        if (!_node || !_node->reversed)
            return;
        _node->reversed = false;
        if (_node->left)
            _node->left->reversed ^= true;
        if (_node->right)
            _node->right->reversed ^= true;
        std::swap(_node->left, _node->right);
        std::swap(_node->maxNotAscendingSuffixLength, _node->maxNotDescendingPrefixLength);
        std::swap(_node->firstValue, _node->lastValue);
    }
    
    CRopePermutableArray::CRopeNode *CRopePermutableArray::update(CRopePermutableArray::CRopeNode *_node) {
        if (_node) {
            push(_node);
            
            _node->size = 1 + size(_node->left) + size(_node->right);
            _node->sum = _node->value + sum(_node->left) + sum(_node->right);
            _node->firstValue = (_node->left ? firstValue(_node->left) : _node->value);
            _node->lastValue = (_node->right ? lastValue(_node->right) : _node->value);
            
            _node->maxNotDescendingPrefixLength = maxNotDescendingPrefixLength(_node->left);
            if (!_node->left || (_node->value >= lastValue(_node->left) && maxNotDescendingPrefixLength(_node->left) == _node->left->size)) {
                _node->maxNotDescendingPrefixLength++;
                if (_node->right && firstValue(_node->right) >= _node->value)
                    _node->maxNotDescendingPrefixLength += maxNotDescendingPrefixLength(_node->right);
            }
            
            _node->maxNotAscendingSuffixLength = maxNotAscendingSuffixLength(_node->right);
            if (!_node->right || (_node->value >= firstValue(_node->right) && maxNotAscendingSuffixLength(_node->right) == _node->right->size)) {
                _node->maxNotAscendingSuffixLength++;
                if (_node->left && lastValue(_node->left) >= _node->value)
                    _node->maxNotAscendingSuffixLength += maxNotAscendingSuffixLength(_node->left);
            }
        }
        return _node;
    }
    
    CRopePermutableArray::CRopeNode *CRopePermutableArray::merge(CRopePermutableArray::CRopeNode *_leftNode, CRopePermutableArray::CRopeNode *_rightNode) {
        if (!_leftNode)
            return _rightNode;
        if (!_rightNode)
            return _leftNode;
        push(_leftNode);
        push(_rightNode);
        if (_leftNode->heapKey < _rightNode->heapKey) {
            _leftNode->right = merge(_leftNode->right, _rightNode);
            return update(_leftNode);
        }
        else {
            _rightNode->left = merge(_leftNode, _rightNode->left);
            return update(_rightNode);
        }
    }
    
    void CRopePermutableArray::split(CRopePermutableArray::CRopeNode *_node, size_t _leftSize, CRopePermutableArray::CRopeNode *&_left, CRopePermutableArray::CRopeNode *&_right) {
        if (!_node) {
            _left = _right = NULL;
            return;
        }
        push(_node);
        if (size(_node->left) < _leftSize) {
            split(_node->right, _leftSize - size(_node->left) - 1, _node->right, _right);
            _left = update(_node);
        }
        else {
            split(_node->left, _leftSize, _left, _node->left);
            _right = update(_node);
        }
    }
    
    void CRopePermutableArray::splitByValue(CRopePermutableArray::CRopeNode *_node, int _value, CRopePermutableArray::CRopeNode *&_left, CRopePermutableArray::CRopeNode *&_right) {
        if (!_node) {
            _left = _right = NULL;
            return;
        }
        push(_node);
        if (_node->value <= _value) {
            splitByValue(_node->right, _value, _node->right, _right);
            _left = update(_node);
        }
        else {
            splitByValue(_node->left, _value, _left, _node->left);
            _right = update(_node);
        }
    }
    
    CRopePermutableArray::CRopeNode *CRopePermutableArray::replaceMinGreater(CRopePermutableArray::CRopeNode *&_node, CRopePermutableArray::CRopeNode *_elem) {
        CRopePermutableArray::CRopeNode *left, *middle, *right;
        splitByValue(_node, _elem->value, left, right);         // nodes in right part greater then _elem->value
        split(right, 1, middle, right);                         // middle is now minimal greater then _elem->value
        right = merge(_elem, right);                            // replacing
        _node = merge(left, right);
        return middle;
    }
    
    void CRopePermutableArray::show(CRopePermutableArray::CRopeNode *_node) {
        if (!_node)
            return;
        push(_node);
        show(_node->left);
        std::cout << _node->value << " ";
        show(_node->right);
    }
    
    
    CRopePermutableArray::CRopePermutableArray() {
        root = NULL;
    }
    
    CRopePermutableArray::~CRopePermutableArray() {
        delete root;
    }
    
    long long CRopePermutableArray::subsegmentSum(size_t _leftEnd, size_t _rightEnd) {
        CRopePermutableArray::CRopeNode *leftPart, *middlePart, *rightPart;
        split(root, _rightEnd, leftPart, rightPart);
        split(leftPart, _leftEnd, leftPart, middlePart);
        long long result = sum(middlePart);
        leftPart = merge(leftPart, middlePart);
        root = merge(leftPart, rightPart);
        return result;
    }
    
    void CRopePermutableArray::insert(size_t _position, int _value) {
        CRopePermutableArray::CRopeNode *leftPart, *rightPart, *newNode;
        split(root, _position, leftPart, rightPart);
        newNode = new CRopePermutableArray::CRopeNode(_value);
        leftPart = merge(leftPart, newNode);
        root = merge(leftPart, rightPart);
    }
    
    void CRopePermutableArray::assign(size_t _position, int _value) {
        CRopePermutableArray::CRopeNode *leftPart, *middlePart, *rightPart;
        split(root, _position, leftPart, rightPart);
        split(rightPart, 1, middlePart, rightPart);
        middlePart->assign(_value);
        rightPart = merge(middlePart, rightPart);
        root = merge(leftPart, rightPart);
    }
    
    void CRopePermutableArray::nextPermutation(size_t _leftEnd, size_t _rightEnd) {
        CRopePermutableArray::CRopeNode *leftPart, *middlePart, *rightPart;
        split(root, _rightEnd, leftPart, rightPart);
        split(leftPart, _leftEnd, leftPart, middlePart);
        size_t maxNotAscendingSuffixLength = middlePart->maxNotAscendingSuffixLength;
        size_t middlePartSize = middlePart->size;
        CRopePermutableArray::CRopeNode *prefix, *middleElem, *suffix;
        split(middlePart, middlePartSize - maxNotAscendingSuffixLength, prefix, suffix);
        suffix->reversed = !suffix->reversed;
        push(suffix);
        if (prefix) {
            split(prefix, size(prefix) - 1, prefix, middleElem);
            middleElem = replaceMinGreater(suffix, middleElem);
            prefix = merge(prefix, middleElem);
        }
        middlePart = merge(prefix, suffix);
        leftPart = merge(leftPart, middlePart);
        root = merge(leftPart, rightPart);
    }
    
    void CRopePermutableArray::show() {
        show(root);
        std::cout << "\n";
    }
    
    void CRopePermutableArray::clear() {
        delete root;
        root = NULL;
    }
}