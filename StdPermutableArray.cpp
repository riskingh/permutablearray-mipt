//
//  StdPermutableArray.cpp
//  PermutableArray
//
//  Created by Максим Гришкин on 15/03/15.
//  Copyright (c) 2015 Максим Гришкин. All rights reserved.
//

#include "StdPermutableArray.h"

namespace NPermutableArray {
    CStdPermutableArray::CStdPermutableArray() {
    }
    
    CStdPermutableArray::~CStdPermutableArray() {
    }
    
    long long CStdPermutableArray::subsegmentSum(size_t _leftEnd, size_t _rightEnd) {
        long long result = 0;
        return std::accumulate(data.begin() + _leftEnd, data.begin() + _rightEnd, result);
    }
    
    void CStdPermutableArray::insert(size_t _position, int _value) {
        std::vector<int>::iterator positionIterator = data.begin() + _position;
        data.insert(positionIterator, _value);
    }
    
    void CStdPermutableArray::assign(size_t _position, int _value) {
        data[_position] = _value;
    }
    
    void CStdPermutableArray::nextPermutation(size_t _leftEnd, size_t _rightEnd) {
        std::vector<int>::iterator leftEndIterator, rightEndIterator;
        leftEndIterator = data.begin() + _leftEnd;
        rightEndIterator = data.begin() + _rightEnd;
        std::next_permutation(leftEndIterator, rightEndIterator);
    }
    
    void CStdPermutableArray::show() {
        for (const auto &elem: data)
            std::cout << elem << " ";
        std::cout << "\n";
    }
    
    void CStdPermutableArray::clear() {
//        std::vector<int> temp;
//        std::swap(data, temp);
        data.clear();
    }
}